package cat.dam.mesa.homersimpson;

import android.graphics.ImageFormat;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    AnimationDrawable titol_animat;
    ImageView iv_homer, iv_titol, iv_ull, iv_everd, iv_evermell, iv_eblau, iv_donut;
    String s = "h";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        titol_animat = new AnimationDrawable();
        iv_titol = (ImageView) findViewById(R.id.iv_titol);
        iv_donut = (ImageView) findViewById(R.id.iv_donut);
        iv_ull = (ImageView) findViewById(R.id.iv_ull);
        iv_everd = (ImageView) findViewById(R.id.iv_everd);
        iv_evermell = (ImageView) findViewById(R.id.iv_evermell);
        iv_eblau = (ImageView) findViewById(R.id.iv_eblau);

        //Animacions
        final Animation animacioVerd = AnimationUtils.loadAnimation(MainActivity.this, R.anim.animacio_everd);
        final Animation animacioUll = AnimationUtils.loadAnimation(MainActivity.this, R.anim.animacio_ull);
        final Animation animacioVermell = AnimationUtils.loadAnimation(MainActivity.this, R.anim.animacio_evermell);
        final Animation animacioBlau = AnimationUtils.loadAnimation(MainActivity.this, R.anim.animacio_eblau);
        final Animation animacioDonut = AnimationUtils.loadAnimation(MainActivity.this, R.anim.animacio_donut);




        //Animació del titol
        iv_titol.setBackgroundResource(R.drawable.titol_animat);
        titol_animat = (AnimationDrawable) iv_titol.getBackground();
        titol_animat.start();

        //Fer que les immatges no hi siguin
        iv_donut.setVisibility(View.GONE);
        iv_evermell.setVisibility(View.GONE);
        iv_everd.setVisibility(View.GONE);
        iv_eblau.setVisibility(View.GONE);
        iv_ull.setVisibility(View.GONE);

        //Fer que es veien al clicar el titol
        iv_titol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    iv_donut.setVisibility(View.VISIBLE);
                    iv_evermell.setVisibility(View.VISIBLE);
                    iv_everd.setVisibility(View.VISIBLE);
                    iv_eblau.setVisibility(View.VISIBLE);
                    iv_ull.setVisibility(View.VISIBLE);

                    //Animacions engranatges
                    iv_eblau.startAnimation(animacioBlau);
                    iv_everd.startAnimation(animacioVerd);
                    iv_evermell.startAnimation(animacioVermell);
                    iv_ull.startAnimation(animacioUll);
                    iv_donut.startAnimation(animacioDonut);


            }
        });
        /*
        //Animacions engranatges
        iv_eblau.startAnimation(animacioBlau);
        iv_everd.startAnimation(animacioVerd);
        iv_evermell.startAnimation(animacioVermell);
        iv_ull.startAnimation(animacioUll);
        iv_donut.startAnimation(animacioDonut);
        */

        //Musica
        MediaPlayer so = MediaPlayer.create(this, R.raw.musica);
        so.start();
    }
}
